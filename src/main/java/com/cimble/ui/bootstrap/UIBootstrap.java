package com.cimble.ui.bootstrap;

import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;

import com.cimble.ui.config.UIBootstrapConfiguration;
import com.cimble.bootstrap.AbstractBootstrap;

/**
 * This class is intended for starting the application. It registers ui module.
 * 
 * @author Hemant Kumar
 * @version 1.0
 */
public class UIBootstrap extends AbstractBootstrap<UIBootstrapConfiguration> {

    public static final String ID = "ui";

    public static void main(String[] args) throws Exception {
        (new UIBootstrap()).run(args);
    }

    @Override
    public void initializeConfig(Bootstrap<UIBootstrapConfiguration> bootstrap) {
        bootstrap.addBundle(new AssetsBundle("/cimble", "/ui/cimble/", "index.html", "cimble"));
    }

    @Override
    public void runBootstrap(UIBootstrapConfiguration configuration, Environment environment) throws Exception {
    	environment.jersey().disable();
    }

    @Override
    public String getId() {
        return ID;
    }

}
