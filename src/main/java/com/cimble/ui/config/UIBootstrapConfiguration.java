package com.cimble.ui.config;

import com.cimble.config.AbstractBootstrapConfiguration;

/**
 * This class is intended for holding all the configurations injected from the external *.yml file.
 * 
 * @author Hemant Kumar
 * @version 1.0
 */
public class UIBootstrapConfiguration extends AbstractBootstrapConfiguration {

}
