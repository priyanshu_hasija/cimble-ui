'use strict';

var rsBaseApp = angular.module('rsBaseApp',
  ['ngResource','ui.router', 'ui.bootstrap', 'LocalStorageModule', 'ngMockE2E','ngCookies'])
  .controller('rsBaseMainCtrl', [
    '$scope','$rootScope', function ($scope,$rootScope) {
    	/*alert("In baseMainCtrl");*/
    	$rootScope.invalidLoginAttempt = 0;
  }
  ]);
rsBaseApp.run([
               '$rootScope', '$location', '$window', '$state','rsAuthFactory','$httpBackend',
             function ($rootScope, $location, $window, $state,rsAuthFactory,$httpBackend) {

             	  /*alert("in app.js followed run");*/
//                 $rootScope.$on("$stateChangeStart", function (event, next, current) {
//                     $rootScope.error = null;
//                     rsAuthFactory.getLocationStorageData();
////                     if (!rsAuthFactory.authorize(next.access)) {
////                    	 alert("authorise-access");
////                         if (rsAuthFactory.isLoggedIn()) {
////                             event.preventDefault();
////                             $state.go($state.current.name);
////                         } else {
////                             event.preventDefault();
////                             $state.go('home.login');
////                         }
//                    	 if(!rsAuthFactory.authentication.isAuth) {
//                    		 event.preventDefault();
////                    		 alert("authenticate");
//                    		 $state.go('home.login');
//                    	 }
//                    	 
////                     }
//
//                   
//                 });
                 $httpBackend.whenGET().passThrough();
                 $httpBackend.whenPOST().passThrough();
                 $httpBackend.whenDELETE().passThrough();
                 $httpBackend.whenPUT().passThrough();
               }
             ]);



