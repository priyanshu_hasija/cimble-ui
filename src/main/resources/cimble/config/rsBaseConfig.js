﻿rsBaseApp.config([
  '$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider',
  function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider) {

      var access = routingConfig.accessLevels;
      //$locationProvider.html5Mode(true).hashPrefix('!');

      $urlRouterProvider.otherwise("404");

      $urlRouterProvider.when('/', '');
      $urlRouterProvider.when('', '/login');

      /*alert("in base-config");*/
      
      $stateProvider
      .state('home', { url: '/', templateUrl: 'views/home/hmFrameCtrl.html', controller: 'hmFrameCtrl', abstract:true,access: [access.anon], friendlyName: 'Frame' ,controllerAs: 'vm'})
      .state('home.resetPassword', {url: 'resetPassword/:token',templateUrl: 'views/authenticate/resetPassword.html', access: [access.anon], controller: 'resetPassword' })
        .state('home.login', { url: 'login', templateUrl: 'views/authenticate/rsAuthLoginCtrl.html', controller: 'rsAuthLoginCtrl', access: [access.anon], controllerAs: 'vm', hideInMenu: true })
        
        .state('home.user', { url: 'user', templateUrl: 'views/home/loginHome.html', controller: 'loginHomeCtrl', abstract:true, access: [access.user], friendlyName: 'User'})
        .state('home.user.device', { url: '/device', templateUrl: 'views/home/deviceDetailsCtrl.html', controller: 'deviceDetailsCtrl',access: [access.user], friendlyName: 'DeviceDetails'})
        .state('home.user.profile', { url: '/profile', templateUrl: 'views/home/profileInfoCtrl.html', controller: 'profileInfoCtrl',access: [access.user], friendlyName: 'UserProfile'})
        
        .state('home.register', { url: 'register', templateUrl: 'views/home/registerHome.html', controller: 'registerHomeCtrl',access: [access.anon], friendlyName: 'Register' ,controllerAs: 'vm'})
        .state('home.404', { url: '404', templateUrl: 'views/home/rs404Ctrl.html', access: [access.public], hideInMenu: true})
        .state('home.401', { url: '401', templateUrl: 'views/home/rs401.html', access: [access.public], hideInMenu: true})
        
//      $httpProvider.interceptors.push('rsAuthInterceptorFactory');
      $httpProvider.defaults.useXDomain = true;
      delete $httpProvider.defaults.headers.common['X-Requested-With'];

  }]);
