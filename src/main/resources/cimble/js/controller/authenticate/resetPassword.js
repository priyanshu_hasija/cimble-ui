/**
 * Reset Password from the link sent via email
 */

rsBaseApp.controller('resetPassword', [
                                       '$scope', '$state','rsValidateTokenFactory','rsDeleteTokenFactory','$interval','rsAuthFactory','rsLoginFactory','rsGetUserLoginDetailsFactory','rsUserProfileFactory','rsRegisterFactory','rsUserRegisterFactory','rsUpdateResetPasswordFactory',
                                       function ($scope, $state,rsValidateTokenFactory,rsDeleteTokenFactory,$interval, rsAuthFactory,rsLoginFactory,rsGetUserLoginDetailsFactory,rsUserProfileFactory,rsRegisterFactory,rsUserRegisterFactory,rsUpdateResetPasswordFactory) {
                                    	   'use strict';

                                    	   var init = function()
                                    	   {
                                    		   console.log($state.params);
                                    		   var token = $state.params.token;
                                    		   //Calling service to validate token
                                    		   rsValidateTokenFactory.get({token:token},function(response){
                                    			   if(response.data != undefined)
                                    			   {
                                    				   /* alert("verified");*/
                                    			   }
                                    			   else
                                    			   {
                                    				   $state.go('home.404');
                                    			   }

                                    		   },function(errorInfo)
                                    		   {

                                    		   });
                                    	   }

                                    	   init();



                                    	   //Reset Password Functionality
                                    	   $scope.showNotEnteredPassword = false;
                                    	   $scope.password = '';
                                    	   console.log($state.params.token);

                                    	   $scope.formData={};

                                    	   $scope.resetPasswordSubmit = function() {
                                    		   console.log($state.params.token);
                                    		   /*alert("In reset Password submit function");*/
                                    		   //checks on empty fields  
                                    		   if(($scope.password=="" || $scope.password==undefined)||($scope.formData.password_c=="" || $scope.formData.password_c==undefined)){
                                    			   $scope.showNotEnteredPassword = true;

                                    		   }else{

                                    			   //Reset Password Submit button function
                                    			   if($scope.password == $scope.formData.password_c){
                                    				   $scope.credentialsResetPassword = {"password":Base64.encode($scope.password)};
                                    				   console.log("post body: "+JSON.stringify($scope.credentialsResetPassword)+"#### token: "+$state.params.token); 

                                    				   //Updating Password in login table
                                    				   rsUpdateResetPasswordFactory.update({token:$state.params.token},$scope.credentialsResetPassword, function(response) {
                                    					   
                                    					   //Delete token from Subscriber_token
                                    					   rsDeleteTokenFactory.update({token:$state.params.token},function(response){
                                    						   $state.go('home.login');
                                    						   alert("Password has been reset !" +
                                    						   "Try Sign In with New Password");
                                    					   },function(errorInfo) {

                                    					   });
                                    				   }, function(errorInfo) {

                                    				   });

                                    			   }else{

                                    				   $scope.myForm.password_c.$error.pwmatch=true;

                                    			   } 


                                    		   }

                                    		   //resetPasswordSubmit end tags  
                                    	   }

                                    	   //Reset Cancel Functionality

                                    	   $scope.resetCancel = function(){

                                    		   $state.go('home.login');
                                    	   }


                                    	   //Reset Password function
                                    	   $scope.resetPassword = function (){

                                    		   /* alert("In reset clear function");*/

                                    		   $scope.password = "";
                                    		   $scope.formData.password_c="";

                                    	   }


                                       }  


                                       ]);

