﻿rsBaseApp.controller('rsAuthLoginCtrl', [
                                         '$scope','$rootScope','localStorageService', '$state', '$interval', 'rsAuthFactory','rsLoginFactory','rsGetUserLoginDetailsFactory','rsUserProfileFactory','rsInsertTokenFactory','rsSendEmailFactory',
                                         function ($scope,$rootScope,localStorageService, $state, $interval, rsAuthFactory,rsLoginFactory,rsGetUserLoginDetailsFactory,rsUserProfileFactory,rsInsertTokenFactory,rsSendEmailFactory) {

                                        	 var init = function()
                                        	 {
                                        		 $scope.invalidCaptcha = false;
                                        		 $scope.emailSent = false;
                                        		 $scope.showmeforget = false;
                                        		 $scope.showLoginForm = true;
                                        		 $scope.myInterval = 4000;
                                        		 if(localStorageService.get('loginDetails') != null && localStorageService.get('loginDetails').username != null)
                                        		 {
                                        			 $scope.name = localStorageService.get('loginDetails').username;
                                        		 }
                                        		 if(localStorageService.get('loginDetails') != null && localStorageService.get('loginDetails').password != null)
                                        		 {
                                        			 //alert(localStorageService.get('loginDetails').password);
                                        			 $scope.pass = localStorageService.get('loginDetails').password;
                                        			 //alert($scope.pass);
                                        		 }
                                        		 $scope.slides = [
                                        		                  {
                                        		                	  image: 'images/bg1.jpg'
                                        		                  },
                                        		                  {
                                        		                	  image:  'images/bg2.jpg'
                                        		                  },
                                        		                  {
                                        		                	  image: 'images/bg3.jpg'
                                        		                  }
                                        		                  ];
                                        	 }

                                        	 init();
                                        	 
                                        	 $scope.incorrectUser = false;
                                        	 
                                        	 //Generate token
                                        	 var generateUUID = function()
                                        	 {
                                        		 var d = new Date().getTime();
                                        		 var uuid = 'xxxxxxxxxxxx4xxxyxxxxxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
                                        			 var r = (d + Math.random()*16)%16 | 0;
                                        			 d = Math.floor(d/16);
                                        			 return (c=='x' ? r : (r&0x3|0x8)).toString(16);
                                        		 });
                                        		 return uuid;
                                        	 }


                                        	 $scope.login = function()
                                        	 { 
                                        		 $scope.credentials = '{"userName":"'+$scope.name+'","password":"'+Base64.encode($scope.pass)+'"}';
                                        		 if($scope.showCaptcha())
                                        		 {
                                        			 if(!$scope.captchaValid)
                                        			 {
                                        				 $scope.invalidCaptcha = true;
                                        				 return; 
                                        			 }
                                        		 }
                                        		 console.log("$scope.credentials"+$scope.credentials);
                                        		 rsLoginFactory.save($scope.credentials, function(
                                        				 response) {
                                        			 if(($scope.name=="" || $scope.name==undefined)||($scope.pass=="" || $scope.pass==undefined)){
                                        				 $scope.showNotEnteredFields = true;
                                        				 return;
                                        			 }
                                        			 console.log("response"+response);
                                        			 if(response.errorCode == 401)
                                        			 {
                                        				 $rootScope.invalidLoginAttempt = $rootScope.invalidLoginAttempt+1;
//                                        				 $scope.showInvalidLogin = true;
                                        				 $state.go('home.login');
                                        				 $scope.showNotEnteredFields = true;
                                        			 }else if(response.data.userName == $scope.name){
                                        				 if($scope.rememberMe)
                                                		 {
                                                			 localStorageService.set('loginDetails',{
                                                	        	  username:$scope.name,
                                                	        	  password:$scope.pass
                                                	          });
                                                		 }
                                        				 var UserSessionDetails = {roles:$scope.name,userName:response.data.userName}
                                        				 rsAuthFactory.setUser(UserSessionDetails);
//                                      				 alert(response.data.id);
                                        				 $rootScope.subscriberId = response.data.id;
                                        				 $state.go('home.user.profile');
                                        			 }

                                        		 }, function(errorInfo) {

                                        		 });

                                        	 }

                                        	 $scope.showCaptcha = function()
                                        	 {
                                        		 if($rootScope.invalidLoginAttempt >= 3)
                                        		 {
                                        			 return true;
                                        		 }
                                        		 else
                                        		 {
                                        			 return false;
                                        		 }
                                        	 }

                                        	 $scope.register = function()
                                        	 { 
                                        		 $state.go('home.register');

                                        	 }
                                        	 $scope.resetFields = function()
                                        	 {
                                        		 $scope.username = '';
                                        		 $scope.name = '';
                                        		 $scope.password = '';
                                        		 $scope.incorrectUser = '';
                                        		 $scope.showCaptcha = false;
                                        	 }

                                        	 //Showing only last 3 digit of user email
                                        	 var confidentialEmailAddress = function(str){
                                        		 var output = '';
                                        		 var strSplit = str.split("@");
                                        		 for(var i=0, len=strSplit[0].length-1; i<=len; i++){
                                        			 if(i < len -2)
                                        			 {
                                        				 output += '*';
                                        			 }
                                        			 else
                                        			 {
                                        				 output += str.charAt(i);
                                        			 }
                                        		 }
                                        		 return output+'@'+strSplit[1];
                                        	 }

                                        	 $scope.forgetPassword = function(){
                                        		 $scope.incorrectUser = false;
                                        		 //Calling service to validate username
                                        		 rsGetUserLoginDetailsFactory.get({username:$scope.username},function(response){
                                        			 if(response.data != undefined && response.data.userName == $scope.username)
                                        			 {

                                        				 //Getting email address for sending reset password URI
                                        				 rsUserProfileFactory.get({username:$scope.username},function(response){
                                        					 var email = response.data.email_address;

                                        					 //Genearting token for password reset
                                        					 var token = generateUUID();
                                        					 var tokenCredentials = {"username":$scope.username,"token":token,"ipAddress":userIp};

                                        					 //inserting token in database
                                        					 rsInsertTokenFactory.save(tokenCredentials,function(response){
                                        						 if(response.data != undefined && response.data.username == $scope.username)
                                        						 {
                                        							 $scope.emailSent = true;
//                                        							 $scope.showmeforget = false;
//                                        							 $scope.showLoginForm = true;
                                        							 $scope.emailAddress = confidentialEmailAddress(email);
                                        							 //Password reset URI to be sent on email
                                        							 var url = "http://54.219.143.75/ui/cimble#/resetPassword/"+token;

                                        							 //Sending mail to user
                                        							 rsSendEmailFactory.get({"email":email,"url":url},function(response){
                                        								 if(response.data != "202")
                                        								 {
                                        									 $scope.emailSent = false;
                                        									 alert("Some error occured while sending email");
                                        									 $state.go("home.404");
                                        								 }
                                        							 },function(errorInfo)
                                        							 {

                                        							 });
                                        						 }else
                                        						 {
                                        							 $scope.incorrectUser = true
                                        						 }
                                        					 },function(errorInfo)
                                        					 {

                                        					 });
                                        				 },function(errorInfo)
                                        				 {

                                        				 });
                                        			 }else
                                        			 {
                                        				 $scope.incorrectUser = true
                                        			 }
                                        		 },function(errorInfo)
                                        		 {

                                        		 });

                                        	 }
                                        	 
                                        	 $scope.backToLogin = function()
                                        	 {
                                        		 $state.go($state.current, {}, {reload: true});
                                        	 }

                                         }
                                         ]);