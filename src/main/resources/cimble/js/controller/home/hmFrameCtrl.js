﻿rsBaseApp.controller('hmFrameCtrl', [
  '$scope','$state', 'rsAuthFactory', '$rootScope', function ($scope,$state, rsAuthFactory,$rootScope) {
      'use strict';

      $scope.authData = rsAuthFactory.authentication;
      
      //#region === Initialize ===
      $scope.getClass = function () {
          if ($state.includes('home.admin.help')) {
              return 'active';
          } else {
              return '';
          }
      }
      $scope.showSideBar = function(){
      	if($state.current.name=='home.monitoring.alarmlist.servedalarm' || $state.current.name=='home.monitoring.alarmlist.unservedalarm' || $state.current.name=='home.monitoring.systemhealth.sitedevices' || $state.current.name=='home.monitoring.systemhealth.sitealarms'){
      		return true;
      	}else{
      		return false;
      	}
      }
      $scope.selectNode = function(currNode) {
  		$scope.selectedNode= currNode;
  		$rootScope.selectedNode = currNode;
  		$scope.$broadcast('dataPassing', currNode);
  	}
      //#endregion
      $scope.logout = function(){
    	  
    	  rsAuthFactory.logout();
    	  $state.go('home.login');
    	  
      }

  }
]);