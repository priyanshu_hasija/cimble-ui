/**
 * Register Home Controller
 */

rsBaseApp.controller('registerHomeCtrl', [
  '$scope', '$state', '$interval','rsAuthFactory','rsLoginFactory','rsGetUserLoginDetailsFactory','rsUserProfileFactory','rsRegisterFactory','rsUserRegisterFactory',
  function ($scope, $state, $interval, rsAuthFactory,rsLoginFactory,rsGetUserLoginDetailsFactory,rsUserProfileFactory,rsRegisterFactory,rsUserRegisterFactory) {
	  'use strict';
  	
	  $scope.showEmptyFieldsMsg = false;
	  $scope.password = '';

	  $scope.formData={};
	  $scope.checkAgree = false;
	  /*alert("on register page");*/

	  $scope.login = function()
	  { 
		  $state.go('home.login');
	  }
  
	  $scope.checkValid = function(){
		  
		 /* alert("Inside check validity");*/
	  }
	  
	   
	//checks on empty fields  
	  $scope.checkFields = function(){
		  /*alert($scope.formData.country);*/
		  if(($scope.formData.firstname=="" || $scope.formData.firstname==undefined)||($scope.formData.lastname=="" || $scope.formData.lastname==undefined)||($scope.formData.email=="" || $scope.formData.email==undefined)||($scope.formData.country=="" || $scope.formData.country==undefined)||($scope.formData.languagecode=="" || $scope.formData.languagecode==undefined)||($scope.username=="" || $scope.username==undefined)||($scope.password=="" || $scope.password==undefined)||($scope.formData.password_c=="" || $scope.formData.password_c==undefined)){
			  
			  $scope.checkAgree = false;
			 /* alert($scope.checkAgree);*/
			  $scope.showEmptyFieldsMsg = true;
			  
			  
		  } 
	  }
  							
	 
	  
	  //register Submit button function
	  $scope.registerSubmit = function(){
		  /*alert("in register submit");*/
          $scope.showEmptyFieldsMsg = false;
          
          /*alert($scope.password);*/
          /*alert($scope.formData.password_c);*/
          
          //if passwords are equal
          if($scope.password == $scope.formData.password_c){
        	  $scope.credentialsLogin = {"userName":$scope.username,"password":Base64.encode($scope.password),"role":"subscriber"};
       	   console.log(JSON.stringify($scope.credentialsLogin));
       	   
       	   
       	   
       	//Adding Login details on Sign Up
       	rsRegisterFactory.register($scope.credentialsLogin, function(
				 response) {
			 if(response.errorCode == 401)
			 {
				 
				 $state.go('home.login');
				 
			 }else {
				 /*alert("login details added");*/
			 }

		 }, function(errorInfo) {

		 });
       	   
       	   
       	//starting registering user details
       	$scope.credentialsUser = {"first_name":$scope.formData.firstname,"last_name":$scope.formData.lastname,"user_name":$scope.username,"email_address":$scope.formData.email,"country_code":$scope.formData.country,"language_code":$scope.formData.languagecode};
  	   console.log(JSON.stringify($scope.credentialsUser));
  	   
  	//Adding User details on Sign Up   
  	 rsUserRegisterFactory.register($scope.credentialsUser, function(
				response) {
  		    /* alert("registering user details");*/
   			if(response.errorCode == 401)
				 {
				 $state.go('home.401');
				 }
		 
  

         }, function(errorInfo) {
	 
	        });
       	
       	
       	
       	   //passwords are equal end tags
          }else{
          	
          	$scope.myForm.password_c.$error.pwmatch=true;
          	
       
          } 
          
          
          
          //register submit button end tag
          
          $state.go('home.login');
          alert("User details have been registered." +
          		"Try Sign In with New Username");
	  }
	  
	  

	  
	  //end tags of main
	  
  								            }
                                          ]);

