﻿// inject REST interceptors here
// Checks for all 401 errors (Unauthorized) returned by 
// any web api calls.  If we received an access deined
// then the user will be directed to a log in page

rsBaseApp.factory('authInterceptorFactory', [
  '$q', '$injector', '$location', '$log', 'localStorageService',
  function ($q, $injector, $location, $log, localStorageService) {
    'use strict';

    //#region === Initialize ===

    var factory = {};

    //#endregion

    //#region === Factory Methods ===

    var _response = function (response) {
      return response;
    }

    var _request = function (config) {

      return config;
    }

    var _responseError = function (rejection) {
      if (rejection.status === 401) {

      }

      return $q.reject(rejection);
    }

    //#endregion

    //#region === Public Interface ===

    factory.response = _response;
    factory.request = _request;
    factory.responseError = _responseError;

    //#endregion

    return factory;
  }
]);