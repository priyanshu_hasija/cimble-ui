rsBaseApp.directive('username', function($q, $timeout , rsGetUserLoginDetailsFactory) {
  return {
    require: 'ngModel',
    link: function(scope, elm, attrs, ctrl) {
    	
    	//call service to cheeck if username exists
    	
    	
    	var usernames=[];
    	    	

      ctrl.$asyncValidators.Username = function(modelValue, viewValue) {
    	  rsGetUserLoginDetailsFactory.get({username:modelValue},function(response){
    			 console.log(response);
    			 if(response.data != undefined && response.data.userName == modelValue)
    			 {
    				usernames = modelValue;
    				 
    			 }
    		 },function(errorInfo)
    		 {

    		 });

        if (ctrl.$isEmpty(modelValue)) {
          // consider empty model valid
          return $q.when();
        }

        var def = $q.defer();

        $timeout(function() {
          // Mock a delayed response
          if (usernames.indexOf(modelValue) === -1) {
            // The username is available
            def.resolve();
          } else {
            def.reject();
          }

        }, 2000);

        return def.promise;
      };
    }
  };
});