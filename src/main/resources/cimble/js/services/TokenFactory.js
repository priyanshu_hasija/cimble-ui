rsBaseApp.factory('rsInsertTokenFactory', ['$http', '$resource', '$q', '$log',
                                     function ($http, $resource, $q, $log) {
	var resource = $resource('/api/v1/token',{},{
		save: {
			method: 'POST'
		}
	});
	return resource;
	
}]);

rsBaseApp.factory('rsValidateTokenFactory', ['$http', '$resource', '$q', '$log',
                                           function ($http, $resource, $q, $log) {
      	var resource = $resource('/api/v1/token/validate/:token',{token:'@token'});
      	return resource;
      	
      }]);

rsBaseApp.factory('rsDeleteTokenFactory',['$http', '$resource', '$q', '$log',
                                          function ($http, $resource, $q, $log) {

  	var resource = $resource('/api/v1/token/:token',{token:'@token'},{
  		update: {
  			method: 'PUT'
  		}
  	});
  	return resource;
  	
  }]);