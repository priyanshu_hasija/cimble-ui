﻿rsBaseApp.factory('rsAuthFactory', ['$http', '$resource', '$q', '$log', 'localStorageService',  '$state',
  function ($http, $resource, $q, $log, localStorageService,  $state) {

      //#region === Initialize ===

      var factory = {};

      var _accessLevels = routingConfig.accessLevels;

      var _userRoles = routingConfig.userRoles;

      var _authentication = { //global variable for maintaing user logged in details
          isAuth: false,
          userName: "",
          roles: [_userRoles.public]
      };


      //#region === Local Methods ===

      var _setUser = function(response) { //response will contain role,username,session_id(if required)
          var roles = response.roles.toLowerCase().split(',');
          var uRoles = [];
          for (var role in roles) {
              if (_userRoles[roles[role]] != undefined)
                  uRoles.push(_userRoles[roles[role]]);
          }
          if (uRoles.length == 0)
              uRoles.push(_userRoles.auth);
//          alert("inside setUser########"+uRoles);
          _authentication.isAuth = true;
          _authentication.userName = response.userName;
          _authentication.roles = uRoles;
          localStorageService.set('authorizationData',{
        	  isAuth:true,
        	  roles:[uRoles]
          });
      };

//      var _authorize = function (accessLevels, roles) {
//
//          var authData = localStorageService.get('authorizationData');
//          if (roles === undefined) {
//              if (authData === null) {
//                  roles = _authentication.roles;
//              } else {
//                  roles = authData.roles;
//              }
//          }
//      }

      var _logout = function () {
    	  localStorageService.remove('authorizationData');

          _authentication.isAuth = false;
          _authentication.userName = "";
          _authentication.roles = [_userRoles.public];
      };

      var _getLocationStorageData = function(){
    	  var localStorageData = localStorageService.get('authorizationData');
    	  if(localStorageData!=null){
        	  _authentication.isAuth = localStorageData.isAuth;
        	  _authentication.roles = localStorageData.roles;
    	  }
      }

      
      
      factory.logout = _logout;
//      factory.authorize = _authorize;
      factory.accessLevels = _accessLevels;
      factory.userRoles = _userRoles;
      factory.setUser = _setUser;
      factory.authentication = _authentication;
      factory.getLocationStorageData = _getLocationStorageData;

      return factory;
  }
]);