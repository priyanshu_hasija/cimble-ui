﻿// inject REST interceptors here
// Checks for all 401 errors (Unauthorized) returned by 
// any web api calls.  If we received an access deined
// then the user will be directed to a log in page

rsBaseApp.factory('rsAuthInterceptorFactory', [
  '$q', '$injector', '$location', '$log', 'localStorageService', '$window',
  function ($q, $injector, $location, $log, localStorageService, $window) {
      'use strict';

      //#region === Initialize ===

      var factory = {};

      //#endregion

      //#region === Factory Methods ===

      var _response = function (response) {
          return response;
      }

      var _request = function (config) {
          config.headers = config.headers || {};

          var authData = localStorageService.get('authorizationData');

          if (authData) {
              config.headers.Authorization = 'Bearer ' + authData.token.access_token;
          }

          return config;
      }

      var _responseError = function (rejection) {
          if (rejection.status === 401) {

              var authService = $injector.get('rsAuthFactory');
              var authData = localStorageService.get('authorizationData');

              if (authData) {
                  if (authData.useRefreshTokens) {
                      $location.path('/refresh');
                      return $q.reject(rejection);
                  }
              }

              authService.logout().then(function (response) {
                  alert("Session has been expired, please relogin & try again.");
                  $window.location.assign('/login');
                  //$location.path('/login');
              }, function (response) {
                  alert("Session has been expired, please relogin & try again.");
                  $window.location.assign('/login');
                  //$location.path('/login');
                  $log.error(response);
              });

          }

          return $q.reject(rejection);
      }

      //#endregion

      //#region === Public Interface ===

      factory.response = _response;
      factory.request = _request;
      factory.responseError = _responseError;

      //#endregion

      return factory;
  }
]);
