/**
 * 
 */

rsBaseApp.factory('rsDeviceDetailsFactory', ['$http', '$resource', '$q', '$log',
  function ($http, $resource, $q, $log) {
	var resource = $resource('/api/v1/user/:user_id/device',{user_id:'@user_id'},{
		method :'GET',
		params :{
			device_id : '@device_id'
		}
	});
      return resource;
  }]);