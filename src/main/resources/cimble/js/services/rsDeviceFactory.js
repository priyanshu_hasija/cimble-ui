/**
 * 
 */

rsBaseApp.factory('rsDeviceFactory', ['$http', '$resource', '$q', '$log',
  function ($http, $resource, $q, $log) {
	var resource = $resource('/api/v1/user/:user_id/device',{user_id:'@user_id'});
      return resource;
  }]);