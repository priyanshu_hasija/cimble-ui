/**
 * 
 */
rsBaseApp.factory('rsGetUserLoginDetailsFactory', ['$http', '$resource', '$q', '$log',
                                                   function ($http, $resource, $q, $log) {
	var resource = $resource('/api/v1/:username',{username:'@username'});
	return resource;
}]);

