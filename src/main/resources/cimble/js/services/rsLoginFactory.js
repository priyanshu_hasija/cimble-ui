/**
 * 
 */
//Sign In login table with username and password
rsBaseApp.factory('rsLoginFactory', ['$http', '$resource', '$q', '$log',
                                     function ($http, $resource, $q, $log) {
	var resource = $resource('/api/v1/signin',{},{
		save: {
			method: 'POST'
		}
	});
	return resource;
	
}]);


// Registering Username and password in login table
rsBaseApp.factory('rsRegisterFactory', ['$http', '$resource', '$q', '$log',
                                     function ($http, $resource, $q, $log) {
	var resource = $resource('/api/v1',{},{
		register: {
			method: 'POST'
		}
	});
	return resource;
	
}]);

rsBaseApp.factory('rsUpdateResetPasswordFactory', ['$http', '$resource', '$q', '$log',
                                                   function ($http, $resource, $q, $log) {

              	var resource = $resource('/api/v1/reset/password/:token',{token:'@token'},{
              		update: {
              			method: 'PUT'
              		}
              	});
              	return resource;
              	
              }]);
