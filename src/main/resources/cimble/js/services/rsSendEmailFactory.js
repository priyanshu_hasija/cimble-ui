rsBaseApp.factory('rsSendEmailFactory', ['$http', '$resource', '$q', '$log',
                                                   function ($http, $resource, $q, $log) {
	var resource = $resource('/api/v1/sendMail',{},{
		Method:'GET',
		params:{
			email:'@email',
			url:'@url'
		}
		
	});
	return resource;
}]);