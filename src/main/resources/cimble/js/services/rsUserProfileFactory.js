/**
 * 
 */

rsBaseApp.factory('rsUserProfileFactory', ['$http', '$resource', '$q', '$log',
  function ($http, $resource, $q, $log) {
	var resource = $resource('/api/v1/user/:username',{username:'@username'});
      return resource;
  }]);


rsBaseApp.factory('rsUserRegisterFactory', ['$http', '$resource', '$q', '$log',
                                     function ($http, $resource, $q, $log) {
	var resource = $resource('/api/v1/user',{},{
		register: {
			method: 'POST'
		}
	});
	return resource;
	
}]);