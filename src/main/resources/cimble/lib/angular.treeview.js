﻿

/*
    angular.treeview.js
*/
(function (l) {
l.module("angularTreeview", []).directive("treeModel", function ($compile) {
        return {
            restrict: "A", link: function (a, g, c) {
            //alert(c.searchQuery);
                var f = c.myEvent;
                var searchQuery = c.searchQuery;
                var e = c.treeModel,h = c.nodeLabel || "label",d = c.nodeChildren || "children",k = '<ul><li data-ng-repeat="node in ' + e +' | filter:'+searchQuery+' "><i class="collapsed" data-ng-show="node.' + d + '.length && node.collapsed" data-ng-click="selectNodeHead(node, $event)"></i><i class="expanded" data-ng-show="node.' + d + '.length && !node.collapsed" data-ng-click="selectNodeHead(node, $event)"></i><i class="normal" data-ng-hide="node.' +
                d + '.length"></i> <span data-ng-class="node.selected" data-ng-click="selectNodeLabel(node, $event)">{{node.' + h + '}}</span><div data-ng-hide="node.collapsed" data-tree-model="node.' + d + '" data-node-id=' + (c.nodeId || "id") + " data-node-label=" + h + " data-node-children=" + d +' data-search-query='+searchQuery+"></div></li></ul>"; e && e.length && (c.angularTreeview ? (a.$watch(e, function (m, b) { g.empty().html($compile(k)(a)) }, !1), a.selectNodeHead = a.selectNodeHead || function (a, b) {
                    b.stopPropagation && b.stopPropagation(); b.preventDefault && b.preventDefault(); b.cancelBubble =
                    !0; b.returnValue = !1; a.collapsed = !a.collapsed
                }, a.selectNodeLabel = a.selectNodeLabel || function (c, b) { b.stopPropagation && b.stopPropagation(); b.preventDefault && b.preventDefault(); b.cancelBubble = !0; b.returnValue = !1; a.currentNode && a.currentNode.selected && (a.currentNode.selected = void 0); c.selected = "selected"; a.currentNode = c;   a[f](c); }) : g.html($compile(k)(a)))
            }
        }
    })
})(angular);

/*l.module('angularTreeview',[]).directive('treeModel',['$compile',function($compile)
                                                      {return{
                                                      restrict:'A',link:function(scope,element,attrs){
                                                      var treeId=attrs.treeId;
                                                      var myevent=attrs.myEvent;
                                                      var searchQuery=attrs.searchQuery;
                                                      var treeModel=attrs.treeModel;
                                                      var nodeId=attrs.nodeId;
                                                      var nodeLabel=attrs.nodeLabel;
                                                      var nodeChildren=attrs.nodeChildren;
                                                      
                                                      var template='<ul>'+'<li data-ng-repeat="node in '+treeModel+' | filter:'+searchQuery+' ">'+'<i class="collapsed" data-ng-class="{nopointer: !node.'+nodeChildren+'.length}"'+'data-ng-show="!node.expanded && !node.fileicon" data-ng-click="'+treeId+'.selectNodeHead(node)"></i>'+'<i class="expanded" data-ng-show="node.expanded && !node.fileicon" data-ng-click="'+treeId+'.selectNodeHead(node)"></i>'+'<i class="normal" data-ng-show="node.fileicon"></i> '+'<span title="{{node.'+nodeLabel+'}}" data-ng-class="node.selected" data-ng-click="'+treeId+'.selectNodeLabel(node)">{{node.'+nodeLabel+'}}</span>'+'<div data-ng-show="node.expanded" data-tree-id="'+treeId+'" data-tree-model="node.'+nodeChildren+'" data-node-id='+nodeId+' data-node-label='+nodeLabel+' data-node-children='+nodeChildren+' data-search-query='+searchQuery+'></div>'+'</li>'+'</ul>';if(treeId&&treeModel){if(attrs.angularTreeview){scope[treeId]=scope[treeId]||{};scope[treeId].selectNodeHead=scope[treeId].selectNodeHead||function(selectedNode){if(selectedNode[nodeChildren]!==undefined){selectedNode.expanded=!selectedNode.expanded;}};scope[treeId].selectNodeLabel=scope[treeId].selectNodeLabel||function(selectedNode){if(scope[treeId].currentNode&&scope[treeId].currentNode.selected){scope[treeId].currentNode.selected=undefined;}
                                                      selectedNode.selected='selected';scope[treeId].currentNode=attrs; scope[myevent](attrs)};}
element.html('').append($compile(template)(scope));}}};}]);})(angular);*/
