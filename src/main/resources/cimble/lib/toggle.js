$(document).ready(function () {
$('[data-toggle=offcanvas]').click(function () {
$parent = $(this).parents(".left-fade").parent();
if ($parent.find('.sidebar-offcanvas').css('background-color') == 'rgb(255, 255, 255)') {
$parent.find('.list-group-item').attr('tabindex', '-1');
} else {
$parent.find('.list-group-item').attr('tabindex', '');
}
$parent.find('.row-offcanvas').toggleClass('active');
});
});